function displayNames(names) {
    for(var i = 0, length = names.length; i < length; i++) {
        console.log(names[i].toUpperCase());
    }
}
var names = [
    'albert', 
    'ALFRED', 
    'Donald', 
    'Jack', 
    'Clarabel', 
    'Fran'
];
displayNames(names);
