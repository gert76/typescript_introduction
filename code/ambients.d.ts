// This is a TypeScript definition file that we create
// to expose the types in ambients.js
// There are no changes to ambients.js
// By declating the interface to ambients.js, we can get complete intellisense support when
// using the code
interface IPerson {
    displayName():void;
    displayFirstName(): void;
}


class Person implements IPerson {
    public first: string;
    public last: string;
    constructor (first: string, last: string);
    displayName():void;
    displayFirstName(): void;
}

// declare mickey as an instance of person
declare var mickey: Person;

// declare donald as an implementation of IPerson and not class Person
// through both mickey and donald are really just instances of Person
// object properties will not show up through intellisense
declare var donald: IPerson;

